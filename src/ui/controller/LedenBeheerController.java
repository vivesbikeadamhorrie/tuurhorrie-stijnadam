/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.controller;

import databag.Lid;
import datatype.Geslacht;
import datatype.Rijksregisternummer;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import transactie.LidTrans;
import ui.VIVESbike;


/**
 * FXML Controller class
 *
 * @author Katrien.Deleu
 */
public class LedenBeheerController implements Initializable {

   
    
// referentie naar VIVESbike (main)
    private VIVESbike parent;
    
    @FXML
    private GridPane grid;
    
    
    @FXML
    private Button lidToevoegen;
    
    @FXML
    private Button lidUitschrijven;
    
    @FXML
    private Button lidWijzigen;
    
    @FXML
    private Button btnannuleren;
    
    @FXML
    private Button btnopslaan;
    
    @FXML
    private TextField rijksregisternummer2;
    
    @FXML
    private TextField voornaam2;
    
    @FXML
    private TextField achternaam;
    
    @FXML
    private TextField email;
    
    @FXML 
    private TextField telefoon;
    
    @FXML
    private DatePicker startDatum;
    
    @FXML
    private CheckBox uitgeschreven;
    
    @FXML
    private RadioButton man;
    
    @FXML
    private RadioButton vrouw;
    
    @FXML
    private TextArea omschrijving;
    
    @FXML
    private TableView listLeden;
    
    @FXML
    private Label error;
    
    @FXML
    private VBox vbox;
    
    @FXML
    private BorderPane bpane;
    
    @FXML
    private Label ledenbeh;
    
    @FXML
    private FlowPane flowpane;
    
    @FXML 
    private FlowPane fpane;
    
    @FXML
    private FlowPane flowpaneregister;
    
    @FXML
    private FlowPane flowpaneregister2;
    
    @FXML
    private GridPane gridpane;
    
    @FXML
    private BorderPane bpane2;
    
   
    private Lid veranderLid = new Lid();
    
    private String whatToDo = "";
    
    private  LidTrans ltr = new LidTrans();
   
    
    /**
     * Referentie naar parent (start) instellen
     *
     * @param parent referentie naar de runnable class die alle oproepen naar de
     * schermen bestuurt
     */
    public void setParent(VIVESbike p) {
        parent = p;
    }
    
    
     /*zet het gripdane op disable*/
    @Override
    public void initialize(URL location, ResourceBundle resources) {
       setError("");
       setTableView();
       grid.setDisable(true);
        
    }
    
    
    /* update lijst met leden*/
    public ObservableList updateLeden()  {
    ObservableList<Lid> data = null;
        
        try {
            data = FXCollections.observableArrayList(ltr.zoekAlleLeden());
        } catch (Exception ex) {
            setError(ex.getMessage());
        } 
        
    return data;
    }
    
    
    /* als er op de knop opslaan word gedrukt*/
    @FXML
    public void aanmakenLid(){
     setError("");
      rijksregisternummer2.setText("");
    achternaam.setText("");
    voornaam2.setText("");
    email.setText("");
    telefoon.setText("");
    startDatum.setValue(null);
    omschrijving.setText("");
        if(!((rijksregisternummer2.getText().trim().equalsIgnoreCase("")) || (achternaam.getText().trim().equalsIgnoreCase("")) || (voornaam2.getText().trim().equalsIgnoreCase("")) || (telefoon.getText().trim().equalsIgnoreCase("")) || (email.getText().trim().equalsIgnoreCase(""))) )
        { Lid l = new Lid();
        Rijksregisternummer nummer = null;
        try {
             nummer = new Rijksregisternummer(rijksregisternummer2.getText());
         
        } catch (Exception ex) {
           setError(ex.getMessage());
        }
     String naam = achternaam.getText();
     String eerstenaam = voornaam2.getText();
     String telnr = telefoon.getText();
     String emailadres = email.getText();
     LocalDate start_lidmaatschap = startDatum.getValue();
     String opmerkingen = omschrijving.getText();
     
     
     if(man.isSelected()){
     l.setGeslacht(Geslacht.M);
     }
     if(vrouw.isSelected()){
     l.setGeslacht(Geslacht.V);
     }
     
     l.setRijksregisternummer(nummer);
     l.setVoornaam(eerstenaam);
     l.setNaam(naam);
     l.setTelnr(telnr);
     l.setEmailadres(emailadres);
     l.setStart_lidmaatschap(start_lidmaatschap);
     l.setOpmerkingen(opmerkingen);
     

        
        try {
            ltr.toevoegenLid(l);
        } catch (Exception ex) {
           setError(ex.getMessage());
        }
        
    rijksregisternummer2.setText("");
    achternaam.setText("");
    voornaam2.setText("");
    email.setText("");
    telefoon.setText("");
    startDatum.setValue(null);
    omschrijving.setText("");
    setTableView();
    grid.setDisable(true);}
        else {
        setError("gelieve de verplichte velden in de vullen");
        }
    }
    
    
    /*als er op de knop lid uitschrijven word gedrukt*/
    @FXML
    public void uitschrijvenLid() {
   setError("");
   Lid l = new Lid();
   l = veranderLid;
   if(l == null) {
   setError("er is geen lid geselecteerd");
   }else {
   
        try {
            ltr.uitschrijvenLid(l.getRijksregisternummer());
        } catch (Exception ex) {
            setError(ex.getMessage());
        } 
     setTableView();}
    
    }
    
    
    /*zet gridpane op enabled*/
    @FXML
    public void enableField() {
   setError("");
        grid.setDisable(false);
    }
    
   @FXML
   public void editLid(){
  
  setError("");
    Lid l = new Lid();
    l = veranderLid;
  
     String naam = achternaam.getText();
     String eerstenaam = voornaam2.getText();
     String telnr = telefoon.getText();
     String emailadres = email.getText();
     LocalDate start_lidmaatschap = startDatum.getValue();
     String opmerkingen = omschrijving.getText();
     
     
     if(man.isSelected()){
     l.setGeslacht(Geslacht.M);
     }
     if(vrouw.isSelected()){
     l.setGeslacht(Geslacht.V);
     }
     
     l.setVoornaam(eerstenaam);
     l.setNaam(naam);
     l.setTelnr(telnr);
     l.setEmailadres(emailadres);
     l.setStart_lidmaatschap(start_lidmaatschap);
     l.setOpmerkingen(opmerkingen);
     
    try {
            ltr.wijzigenLid(l);
        } catch (Exception ex) {
            setError(ex.getMessage());
        } 
        grid.setDisable(false);
       setTableView();
   }
    
   /*zet alle leden in de tableView*/
    public void setTableView() {
    setError("");
    ObservableList data = FXCollections.observableList(updateLeden());
    listLeden.setItems(data);
    
    }
   
    /*toont het lid */
    public void showLid() {
   setError("");
        Lid l = new Lid();
    l = (Lid) listLeden.getSelectionModel().getSelectedItem();
    veranderLid=l;
         rijksregisternummer2.setText(l.getRijksregisternummer());
    achternaam.setText(l.getNaam());
    voornaam2.setText(l.getVoornaam());
    email.setText(l.getEmailadres());
    telefoon.setText(l.getTelnr());
    startDatum.setValue(l.getStart_lidmaatschap());
    omschrijving.setText(l.getOpmerkingen());
    if(l.getGeslacht().equals("M")){
    man.setSelected(true);
    }
    else {
    vrouw.setSelected(true);
    }
    }
    
    public void opslaan() {
    setError("opslaan uitgevoerd" + whatToDo);
        if(whatToDo.equalsIgnoreCase("new")){
    aanmakenLid();
    }
    if(whatToDo.equalsIgnoreCase("edit")){
    editLid();
    }
    
    }
    /*Als er op de knop annuleren word geduwd*/
    public void annuleren() {
   setError("");
    
    rijksregisternummer2.setText("");
    achternaam.setText("");
    voornaam2.setText("");
    email.setText("");
    telefoon.setText("");
    startDatum.setValue(null);
    omschrijving.setText("");
    
    
    grid.setDisable(true);
    setError("de wijzigingen zijn niet opgeslaan");
    }
    
    public void toevoegen(){
        setError("");
        enableField();
        whatToDo = "new";
}
   
    
    public void edit() {
     whatToDo="edit";   
     setError("edit uitgevoerd, whatToDo: " + whatToDo);
     //setError("");
     grid.setDisable(false);
     veranderLid = (Lid) listLeden.getSelectionModel().getSelectedItem();
     
    }
    
    /*stel de error in*/
    public void setError(String message){
    error.setText(message);
    }
    
}

    

