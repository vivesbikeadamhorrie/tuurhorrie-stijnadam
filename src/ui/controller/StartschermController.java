/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import ui.VIVESbike;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ui.controller.LedenBeheerController;


/**
 * FXML Controller class
 *
 * @author Martijn
 */
public class StartschermController implements Initializable {
@FXML
private Button leden;

@FXML
private Button  fietsen;

@FXML
private Button ritten;

@FXML
private AnchorPane pane;

@FXML
private Label lab;

@FXML
Scene LedenBeheer;

    // referentie naar VIVESbike (main)
    private VIVESbike parent;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        setParent(new VIVESbike());
    }

    /**
     * Referentie naar parent (start) instellen
     *
     * @param parent referentie naar de runnable class die alle oproepen naar de
     * schermen bestuurt
     */
    public void setParent(VIVESbike p) {
        parent = p;
    }
        
    public void gotoLedenBeheer() {
      
        parent.laadLedenbeheer();
        parent.getPrimaryStage().show();
    }

}
