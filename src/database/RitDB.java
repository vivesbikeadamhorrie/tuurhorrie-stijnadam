package database;

import com.mysql.jdbc.Statement;
import databag.Rit;
import database.connect.ConnectionManager;
import datatype.Rijksregisternummer;
import exception.ApplicationException;
import exception.DBException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public class RitDB implements InterfaceRitDB {

    /**
     * Voegt een rit toe
     *
     * @param rit rit die moet toegevoegd worden
     * @return id van de pas toegevoegde rit
     */
    @Override
    public Integer toevoegenRit(Rit rit) throws DBException, SQLException  {
        if (rit != null) {
            
            Integer primaryKey = null;
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "insert into rit("
                        + "starttijd"
                        + " , prijs"
                        + " , lid_rijksregisternummer"
                        + " , fiets_registratienummer"
                        + " ) values(?,?,?,?)",Statement.RETURN_GENERATED_KEYS
                       );) {
                    stmt.setTimestamp(1, Timestamp.valueOf(rit.getStarttijd()));
                    stmt.setBigDecimal(2, rit.getPrijs());
                    stmt.setString(3, rit.getLidRijksregisternummer());
                    stmt.setInt(4, rit.getFietsRegistratienummer());
                    stmt.execute();
                    
                    //de door de database aangemaakte registratienummer opvragen en in een variable stoppen
                    ResultSet generatedKeys = stmt.getGeneratedKeys();
                    if (generatedKeys.next()){
                        primaryKey = generatedKeys.getInt(1);
                    }

                    return primaryKey;
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in toevoegenRit " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in toevoegenRit " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in toevoegenRit - ongeldige paramter(s)");}
        
        }
        return null;
    }

    /**
     * Sluit een rit af.
     *
     * @param rit rit die afgesloten moet worden
     */
    @Override
    public void afsluitenRit(Rit rit) throws DBException, SQLException {
        if (rit != null) {
            
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "update rit set " 
                        + "eindtijd = ?"
                        + " where id = " + rit.getRitID().toString()
                        );) {
                            stmt.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
                            stmt.execute();
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in afsluitenRit " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in afsluitenRit " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-exception in afsluitenRit er is een ongeldige parameter");}
        }
    }

    /**
     * Zoekt alle ritten.
     *
     * @return een arraylist met alle Rit-objecten. Een lege lijst indien er
     * geen ritten gevonden zijn.
     */
    @Override
    public ArrayList zoekAlleRitten() throws DBException, SQLException, ApplicationException  {
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "select * from rit"
                        );) {
                            stmt.execute();
                        ResultSet r = stmt.getResultSet();
                    ArrayList<Rit> alleritten = new ArrayList<Rit>();
                    //lijst opvullen met alle ritten
                    try{
                        while(r.next()){
                            Rit rit = new Rit();

                            //alle ritten overlopen en uiteindelijk toevoegen aan de lijst "alleritten"
                            rit.setRitID(r.getInt("id"));
                            rit.setStarttijd(r.getTimestamp("starttijd").toLocalDateTime());
                            //eindtijd invullen als de eindtijd niet null is in de database
                            if(r.getTimestamp("eindtijd") != null){rit.setEindtijd(r.getTimestamp("eindtijd").toLocalDateTime());}
                            //prijs invullen als de prijs niet null is in de database
                            if(r.getBigDecimal("prijs") != null){rit.setPrijs(r.getBigDecimal("prijs"));}

                            Rijksregisternummer rnr = new Rijksregisternummer(r.getString("lid_rijksregisternummer")); //deze lijn kan applicatie exception gooien
                            rit.setLidRijksregisternummer(rnr);

                            rit.setFietsRegistratienummer(r.getInt("fiets_registratienummer"));
                            
                            alleritten.add(rit);

                        }
                        return alleritten;
                    }
                    catch(ApplicationException e){throw new ApplicationException("exception in zoekAlleRitten - aaplication " + e.getMessage());}
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in zoekAlleRitten " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in zoekAlleRitten " + "- connection" + sqlEx.getMessage()); }
        
    }

    
    /**
     *
     * Zoekt een rit
     *
     * @param ritID id van de te zoeken rit
     * @return Rit-object met alle gegevens van de gevonden rit. Null indien de
     * rit niet bestaat.
     */
    @Override
    public Rit zoekRit(Integer ritID) throws DBException, SQLException, ApplicationException   {
        try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "select * from rit where id = " + ritID
                        );) {
                            stmt.execute();
                        ResultSet r = stmt.getResultSet();
                    try{
                        while(r.next()){
                            Rit rit = new Rit();

                            rit.setRitID(r.getInt("id"));
                            rit.setStarttijd(r.getTimestamp("starttijd").toLocalDateTime());
                            //eindtijd invullen als de eindtijd niet null is in de database
                            if(r.getTimestamp("eindtijd") != null){rit.setEindtijd(r.getTimestamp("eindtijd").toLocalDateTime());}
                            //prijs invullen als de prijs niet null is in de database
                            if(r.getBigDecimal("prijs") != null){rit.setPrijs(r.getBigDecimal("prijs"));}

                            Rijksregisternummer rnr = new Rijksregisternummer(r.getString("lid_rijksregisternummer")); //deze lijn kan applicatie exception gooien
                            rit.setLidRijksregisternummer(rnr);

                            rit.setFietsRegistratienummer(r.getInt("fiets_registratienummer"));
                            return rit;
                        }
                    }
                    catch(ApplicationException e){throw new ApplicationException("exception in zoekRit - aplication " + e.getMessage());}
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in zoekRit " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in zoekRit " + "- connection" + sqlEx.getMessage()); }
        return null;
    }

    /**
     * Zoekt het id van de eerste rit van een lid
     *
     * @param rr rijksregeisternummer van het lid waarvan de eerste rit gezocht
     * wordt
     * @return id van de eerste rit van een lid
     */
    @Override
    public Integer zoekEersteRitVanLid(String rr) throws DBException, SQLException {
        if (rr != null) {
            
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "select * from rit where lid_rijksregisternummer = " + rr +" order by starttijd asc limit 1"//--> alternatieve oplossing: op het einde "limit 1" schrijven en "top 1" wegdoen
                        );) {
                            stmt.execute();
                            ResultSet r = stmt.getResultSet();
                            while(r.next()){
                            return r.getInt("id");
                            }
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in zoekEersteRitVanLid " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in zoekEersteRitVanLid " + "- connection" + sqlEx.getMessage()); }
        }
        return null;
    }

    /**
     * Zoekt de actieve ritten van een lid (dus alle openstaande ritten op dit
     * moment van een lid).
     *
     * @param rr rijksregeisternummer van het lid waarvan de actieve ritten
     * worden gezocht
     * @return een arraylist van alle actieve ritten van een gegeven lid
     */
    @Override
    public ArrayList zoekActieveRittenVanLid(String rr) throws DBException, SQLException, ApplicationException  {
        if (rr != null) {
            
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "select * from rit where lid_rijksregisternummer = " + rr + " AND eindtijd IS NULL"
                        );) {
                            stmt.execute();
                            ResultSet r = stmt.getResultSet();
                            
                        ArrayList<Rit> alleritten = new ArrayList<Rit>();
                                try{
                                    while(r.next()){
                                        Rit rit = new Rit();

                                        //alle ritten overlopen en uiteindelijk toevoegen aan de lijst "alleritten"
                                        rit.setRitID(r.getInt("id"));
                                        rit.setStarttijd(r.getTimestamp("starttijd").toLocalDateTime());
                                        //prijs invullen als de prijs niet null is in de database
                                        if(r.getBigDecimal("prijs") != null){rit.setPrijs(r.getBigDecimal("prijs"));}

                                        Rijksregisternummer rnr = new Rijksregisternummer(rr); //deze lijn kan applicatie exception gooien
                                        rit.setLidRijksregisternummer(rnr);
                                        rit.setFietsRegistratienummer(r.getInt("fiets_registratienummer"));

                                        alleritten.add(rit);

                                    }
                                    return alleritten;
                                }
                                catch(ApplicationException e){throw new ApplicationException("exception in ZoekActieveRittenVanLid - aplication " + e.getMessage());}
                    
                            
                } 
                catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in zoekActieveRittenVanLid " + "- statement " + sqlEx.getMessage());
                }
            }
            catch (SQLException sqlEx){throw new DBException("SQL-exception in zoekActieveRittenVanLid " + "- connection" + sqlEx.getMessage()); }
        }
        return null;
    }

    /**
     * Zoekt de actieve ritten van een fiets (dus alle openstaande ritten op dit
     * moment van een fiets).
     *
     * @param regnr registernummer van de fiets waarvan de actieve ritten worden
     * gezocht
     * @return een arraylist van alle actieve ritten van een gegeven fiets
     */
    @Override
    public ArrayList zoekActieveRittenVanFiets(Integer regnr) throws DBException, SQLException, ApplicationException  {
        if (regnr != null) {
            
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "select * from rit where fiets_registratienummer = " + regnr + " AND eindtijd IS NULL"
                        );) {
                            stmt.execute();
                            ResultSet r = stmt.getResultSet();
                            
                        ArrayList<Rit> alleritten = new ArrayList<Rit>();
                                try{
                                    while(r.next()){
                                        Rit rit = new Rit();

                                        //alle ritten overlopen en uiteindelijk toevoegen aan de lijst "alleritten"
                                        rit.setRitID(r.getInt("id"));
                                        rit.setStarttijd(r.getTimestamp("starttijd").toLocalDateTime());
                                        //prijs invullen als de prijs niet null is in de database
                                        if(r.getBigDecimal("prijs") != null){rit.setPrijs(r.getBigDecimal("prijs"));}

                                        Rijksregisternummer rnr = new Rijksregisternummer(r.getString("lid_rijksregisternummer")); //deze lijn kan applicatie exception gooien
                                        rit.setLidRijksregisternummer(rnr);
                                        rit.setFietsRegistratienummer(r.getInt("fiets_registratienummer"));

                                        alleritten.add(rit);

                                    }
                                    return alleritten;
                                }
                                catch(ApplicationException e){throw new ApplicationException("exception in ZoekActieveRittenVanFiets - aplication " + e.getMessage());}
                    
                            
                } 
                catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in ZoekActieveRittenVanFiets " + "- statement " + sqlEx.getMessage());
                }
            }
            catch (SQLException sqlEx){throw new DBException("SQL-exception in ZoekActieveRittenVanFiets " + "- connection" + sqlEx.getMessage()); }
        }
        return null;
    }

   
}
