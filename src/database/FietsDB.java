package database;

import com.mysql.jdbc.Statement;
import databag.Fiets;
import database.connect.ConnectionManager;
import datatype.Standplaats;
import datatype.Status;
import exception.DBException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FietsDB implements InterfaceFietsDB {

    /**
     * Voegt een fiets toe
     *
     * @param fiets fiets die moet toegevoegd worden
     * @return id van de pas toegevoegde fiets
     */
    @Override
    public Integer toevoegenFiets(Fiets fiets) throws DBException, SQLException    {
        if (fiets != null) {
            
            Integer primaryKey = null;
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "insert into fiets(" 
                        + " status"
                        + " , standplaats"
                        + " , opmerkingen"
                        + " ) values(?,?,?)",Statement.RETURN_GENERATED_KEYS
                       );) {
                    //stmt.setInt(1, fiets.getRegistratienummer());
                    stmt.setString(1, fiets.getStatus().toString()); //setstring? bij enum ---------------------------------------------
                    stmt.setString(2, fiets.getStandplaats().toString()); //setstring? bij enum ---------------------------------------------
                    stmt.setString(3, fiets.getOpmerking()); //setstring? bij text ---------------------------------------------
                    stmt.execute(); 
                    
                    
                    //de door de database aangemaakte registratienummer opvragen en in een variable stoppen
                    ResultSet generatedKeys = stmt.getGeneratedKeys();
                    if (generatedKeys.next()){
                        primaryKey = generatedKeys.getInt(1);
                    }
                    
                    return primaryKey;
                } catch (SQLException sqlEx) {
                    throw new DBException("DB-exception in toevoegenFiets " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in toevoegenFiets " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in toevoegenFiets - ongeldige paramter(s)");}
        }
        return null;
    }

    /**
     * Wijzigt de status van een fiets
     *
     * @param regnr registratienummer van de te wijzigen fiets
     * @param status nieuwe status van de fiets
     */
    @Override
    public void wijzigenToestandFiets(Integer regnr, Status status) throws DBException, SQLException   {
        if (regnr != null && status != null) {
            try (Connection conn = ConnectionManager.getConnection();) {
                
                try (PreparedStatement stmt = conn.prepareStatement(
                        "update fiets set status = '" + status.toString()
                        + "' where registratienummer = " + regnr.toString()
                       );) {
                    stmt.execute(); 
                } catch (SQLException sqlEx) {
                    throw new DBException("DB-exception in wijzigenToestandFiets " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in wijzigenToestandFiets " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in wijzigenFiets - ongeldige paramter(s)");}
        }
    }

    /**
     * Wijzigt de opmerking van een fiets
     *
     * @param regnr registratienymmer van de te wijzigen fiets
     * @param opmerking nieuwe opmerking die toegevoegd moet worden aan de fiets
     */
    @Override
    public void wijzigenOpmerkingFiets(Integer regnr, String opmerking) throws DBException, SQLException   {
        if (regnr != null && opmerking != null) {
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "update fiets set opmerkingen = '" + opmerking.toString()
                        + "' where registratienummer = " + regnr.toString()
                       );) {
                    stmt.execute(); 
                } catch (SQLException sqlEx) {
                    throw new DBException("DB-exception in wijzigenOpmerkingFiets " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in wijzigenOpmerkingFiets " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in wijzigenopmerkingFiets - ongeldige paramter(s)");}
        }
    }

    /**
     * Zoekt een fiets
     *
     * @param regnr registratienummer van de te zoeken fiets
     * @return Fiets-object met alle gegevens van de gevonden fiets. Null indien
     * de fiets niet bestaat.
     */
    @Override
    public Fiets zoekFiets(Integer regnr) throws DBException, SQLException   {
        
        
        if (regnr != null) {
            Fiets returnFiets = null;
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "select * from fiets" 
                        + " where registratienummer = " + regnr 
                       );) {
                    stmt.execute();
                    
                    //resultaten opvragen (en automatisch sluiten)
                    try (ResultSet r = stmt.getResultSet()){
                        //van fiets uit de database een Fiets-object maken
                        Fiets f = new Fiets();
                        if(r.next()){
                                f.setRegistratienummer(regnr);
                                f.setStatus(Status.valueOf(r.getString("status")));
                                f.setStandplaats(Standplaats.valueOf(r.getString("standplaats")) );
                                //enkel de opmerking instellen als ze niet null is in de database
                                if(r.getString("opmerkingen") != null){f.setOpmerking(r.getString("opmerkingen"));}
                                returnFiets = f;
                                return returnFiets;
                            }
                        }                 
                       
                    } catch (SQLException sqlEx) {
                    throw new DBException("DB-exception in zoekFiets " + "- statement " + sqlEx.getMessage());
                    }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in zoekFiets" + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in zoekFiets - ongeldige paramter(s)");}
        }
        //regnr was null, return null
        return null;
        
        
    }

    /**
     * Zoekt alle fietsen.
     *
     * @return een arraylist met alle Fiets-objecten. Een lege lijst indien er
     * geen fietsen gevonden zijn.
     */
    @Override
    public ArrayList<Fiets> zoekAlleFietsen() throws DBException, SQLException   {
        

            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "select * from fiets"
                       );) {
                    stmt.execute();
                    
                    //resultaten opvragen (en automatisch sluiten)
                    try (ResultSet r = stmt.getResultSet()){
                        ArrayList<Fiets> alleFietsen = new ArrayList<Fiets>();
                        //van fiets uit de database een Fiets-object maken

                        while(r.next()){
                                Fiets f = new Fiets();
                                f.setRegistratienummer(r.getInt("registratienummer"));
                                f.setStatus(Status.valueOf(r.getString("status")));
                                f.setStandplaats(Standplaats.valueOf(r.getString("standplaats")) );
                                if(r.getString("opmerkingen") != null){f.setOpmerking(r.getString("opmerkingen"));}
                                
                                alleFietsen.add(f);
                            }
                        return alleFietsen;
                        }                 
                       
                    } catch (SQLException sqlEx) {
                    throw new DBException("DB-exception in zoekAlleFietsen " + "- statement " + sqlEx.getMessage());
                    }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in zoekAlleFietsen " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in zoekAlleFietsen - ongeldige paramter(s)");}

        
        
    }

   
}
