package database;
import databag.Lid;
import java.util.ArrayList;
    import java.util.Date;
    import java.sql.Connection;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
import java.sql.SQLException;
import database.connect.ConnectionManager;
import datatype.Geslacht;
import datatype.Rijksregisternummer;
import exception.ApplicationException;
import exception.DBException;
import java.time.ZoneId;
import java.text.SimpleDateFormat;

public class LidDB implements InterfaceLidDB {

    /**
     * Voegt een lid toe
     *
     * @param lid lid dat toegevoegd moet worden
     */
    @Override
    public void toevoegenLid(Lid lid) throws DBException, SQLException {
        if (lid != null) {
            
            try (Connection conn = ConnectionManager.getConnection();) {
                
                //de huidige datum in het juiste formaat maken
                Date nu = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                String formatnu = formatter.format(nu);
                
                try (PreparedStatement stmt = conn.prepareStatement(
                        
                        "insert into lid(" 
                        + " rijksregisternummer"
                        + " , voornaam"
                        + " , naam"
                        + " , geslacht"
                        + " , telnr"
                        + " , emailadres"
                        + " , start_lidmaatschap"
                        + " , opmerkingen"        
                        + " ) values(?,?,?,?,?,?," + formatnu + " , ?) "
                       );) {
                    
                    stmt.setString(1, lid.getRijksregisternummer());
                    stmt.setString(2, lid.getVoornaam());
                    stmt.setString(3, lid.getNaam());
                    stmt.setString(4, lid.getGeslacht().name());
                    stmt.setString(5, lid.getTelnr());
                    stmt.setString(6, lid.getEmailadres());
                    stmt.setString(7, lid.getOpmerkingen());
                    
                    stmt.execute();

                    
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in toevoegenLid " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in toevoegenLid " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in toevoegenLid - ongeldige paramter(s)");}
        }
        
        }
    

    /**
     * Wijzigt een lid
     *
     * @param lid het nieuwe lid (rijksregisternummer kan niet gewijzigd worden)
     */
    @Override
    public void wijzigenLid(Lid lid) throws DBException, SQLException{
        
        if (lid != null) {
            
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "update lid set"
                               
                        + " voornaam = ?"
                        + " , naam = ?"
                        + " , geslacht = ?"
                        + " , telnr = ?"
                        + " , emailadres = ?"
                        + " , start_lidmaatschap = ?"
                        + " where rijksregisternummer = " + lid.getRijksregisternummer()
                       );) {
                    
                    stmt.setString(1, lid.getVoornaam());
                    stmt.setString(2, lid.getNaam());
                    stmt.setString(3, lid.getGeslacht().name());
                    stmt.setString(4, lid.getTelnr());
                    stmt.setString(5, lid.getEmailadres());
                    stmt.setDate(6, java.sql.Date.valueOf(lid.getStart_lidmaatschap()) );
                    stmt.execute();

                    
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in wijzigenlid " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in wijzigenLid " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in wijzigenLid - ongeldige paramter(s)");}
        }
        
        
    }

    /**
     * Schrijft een lid uit.
     *
     * @param rr rijksregisternummer van het uit te schrijven lid
     */
    @Override
    public void uitschrijvenLid(String rr) throws DBException, SQLException{
        if (rr != null) {
            
            try (Connection conn = ConnectionManager.getConnection();) {
                
                //de huidige datum in het juiste formaat maken
                Date nu = new Date();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                String formatnu = formatter.format(nu);
                
                try (PreparedStatement stmt = conn.prepareStatement(
                        "update lid set" 
                        + "  einde_lidmaatschap = " + formatnu
                        + " where rijksregisternummer = " + rr 
                       );) {
                    stmt.execute();

                    
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in uitschrijvenLid " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in uitschrijvenLid " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in uitschrijvenLid - ongeldige paramter(s)");}
        }
        
    }

    /**
     * Zoekt een lid.
     *
     * @param rijksregisternummer het rijksregisternummer van het te zoeken lid
     * @return Lid-object met alle gegevens van het gevonden lid. Null indien
     * het lid niet bestaat.
     */
    @Override
    public Lid zoekLid(String rijksregisternummer) throws DBException, SQLException, ApplicationException{
        if (rijksregisternummer != null) {
            Lid returnLid = null;
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        "select * from lid" 
                        + " where rijksregisternummer = " + rijksregisternummer
                       );) {
                    stmt.execute();
                    
                    //resultaten opvragen (en automatisch sluiten)
                    try (ResultSet r = stmt.getResultSet()){
                        //van lid uit de database een lid-object maken
                        Lid l = new Lid();
                        //er werd een klant gevonden
                        if(r.next()){
                            try{
                            
                                Rijksregisternummer rnr = new Rijksregisternummer(r.getString("rijksregisternummer")); //deze lijn kan applicatie exception gooien
                                l.setRijksregisternummer(rnr);
                                l.setVoornaam(r.getString("voornaam"));
                                l.setNaam(r.getString("naam"));
                                l.setGeslacht(Geslacht.valueOf(r.getString("geslacht")));
                                l.setTelnr(r.getString("telnr"));
                                l.setEmailadres(r.getString("emailadres"));
                                l.setStart_lidmaatschap(r.getDate("start_lidmaatschap").toLocalDate());
                                //enkel einddatum instellen als het niet null is in de database
                                if(r.getDate("einde_lidmaatschap")!= null){l.setEinde_lidmaatschap(r.getDate("einde_lidmaatschap").toLocalDate());}
                                //enkel de opmerking instellen als het niet null is in de database
                                if(r.getString("opmerkingen") != null){l.setOpmerkingen(r.getString("opmerkingen"));}
                                
                                returnLid = l;
                                return returnLid;
                            }
                            catch(ApplicationException e){throw new ApplicationException("exception in zoekLid - aaplication " + e.getMessage());}
                        }                 
                    }      
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in zoekLid " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in zoekLid " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in zoekLid - ongeldige paramter(s)");}
        }
        //geen rijksregister megegeven, geen lid gevonden (geen klant gemaakt om te returnen --> returned null)
        return null;
    }

    /**
     * Zoekt alle leden.
     *
     * @return een arraylist met alle Lid-objecten. Een lege lijst indien er
     * geen leden gevonden zijn.
     */
    @Override
    public ArrayList<Lid> zoekAlleLeden() throws DBException, SQLException, ApplicationException {
            try (Connection conn = ConnectionManager.getConnection();) {
                try (PreparedStatement stmt = conn.prepareStatement(
                        
                        "select * from lid" 
                       );) {
                    stmt.execute();
                    try(ResultSet r = stmt.getResultSet();){
                        ArrayList<Lid> alleLeden = new ArrayList<Lid>();

                        //lijst opvullen met alle leden
                        while(r.next()){
                            Lid l = new Lid();

                            //lid per lid overlopen een een object van maken en dan toevoegen aan de lijst
                            try{
                                    Rijksregisternummer rnr = new Rijksregisternummer(r.getString("rijksregisternummer")); //deze lijn kan applicatie exception gooien
                                    l.setRijksregisternummer(rnr);
                                    l.setVoornaam(r.getString("voornaam"));
                                    l.setNaam(r.getString("naam"));
                                    l.setGeslacht(Geslacht.valueOf(r.getString("geslacht")));
                                    l.setTelnr(r.getString("telnr"));
                                    l.setEmailadres(r.getString("emailadres"));
                                    l.setStart_lidmaatschap(r.getDate("start_lidmaatschap").toLocalDate());
                                    //enkel einddatum instellen als het niet null is in de database
                                    if(r.getDate("einde_lidmaatschap")!= null){l.setEinde_lidmaatschap(r.getDate("einde_lidmaatschap").toLocalDate());}
                                    //enkel de opmerking instellen als het niet null is in de database
                                    if(r.getString("opmerkingen") != null){l.setOpmerkingen(r.getString("opmerkingen"));}
                                }
                            catch(ApplicationException e){throw new ApplicationException("exception in zoekAlleLeden - application " + e.getMessage());}

                            alleLeden.add(l);
                        }
                        //de lijst met alle leden erin returnen
                        return alleLeden;
                    }
                } catch (SQLException sqlEx) {
                    throw new DBException("SQL-exception in zoekAlleLeden " + "- statement " + sqlEx.getMessage());
                }
            }catch (SQLException sqlEx){throw new DBException("SQL-exception in zoekAlleLeden " + "- connection" + sqlEx.getMessage()); }
            catch(Exception e){throw new DBException("SQL-Exception in zoekAlleLeden - ongeldige paramter(s)");}
        
            
    }

}
