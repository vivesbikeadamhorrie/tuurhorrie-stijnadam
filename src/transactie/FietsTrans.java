
package transactie;

import databag.Fiets;
import databag.Rit;
import database.FietsDB;
import database.RitDB;
import datatype.Status;
import exception.ApplicationException;
import exception.DBException;
import java.sql.SQLException;
import java.util.ArrayList;

public class FietsTrans implements InterfaceFietsTrans {

    private FietsDB db = new FietsDB();
    private RitDB rdb = new RitDB();
    
    /**
     * Voegt een fiets toe
     *
     * @param fiets fiets die moet toegevoegd worden
     * @return id van de pas toegevoegde fiets
     */
    @Override
    public Integer toevoegenFiets(Fiets fiets) throws ApplicationException, DBException, SQLException  {
        if(fiets == null){throw new ApplicationException("er werd geen fiets megegeven");} //de megegeven fiets is null
        
        //zijn alle nodige velden ingevuld?, standplaats, opmerking en status mogen niet null zijn
        if(fiets.getStandplaats() == null){
            throw new ApplicationException("standplaats is niet ingevuld");
        }
        if(fiets.getOpmerking() == null){
            throw new ApplicationException("opmerking mag leeg zijn, maar niet null");
        }
        if(fiets.getStatus()== null){
            throw new ApplicationException("status is niet ingevuld");
        }
        
        //de fiets toevoegen
        Integer i = db.toevoegenFiets(fiets); //-> dit kan DB en SQL Exceptions gooien
        
        return i;
    }

    /**
     * Wijzigt de status van een actieve fiets naar herstel
     *
     * @param regnr registratienummer van de te wijzigen fiets
     */
    @Override
    public void wijzigenActiefNaarHerstel(Integer regnr) throws DBException, SQLException, ApplicationException  {
        //kijken of de fiets momenteel op actief staat
        Fiets fiets = new Fiets();
        fiets = db.zoekFiets(regnr);
        
        //nagaan of er momenteel geen actieve ritten zijn met deze fiets
        ArrayList<Rit> rittenlijst = rdb.zoekActieveRittenVanFiets(fiets.getRegistratienummer());
        if(rittenlijst.isEmpty()){
            if(fiets.getStatus() == Status.actief){
                //de fiets is actief

                //toestand van de fiets wijzigen naar herstel
                db.wijzigenToestandFiets(regnr, Status.herstel);
            }
            else{
                //de fiets was niet actief
                throw new ApplicationException("kan niet van actief naar herstel zetten, want fiets is momenteel niet actief");
            }
        }
        else{
            //er waren nog actieve ritten met de fiets
            throw new ApplicationException("er zijn nog steed actieve ritten met deze fiets, kan de fiets niet naar uitomloop zetten");
        }
        
    }

    
    /**
     * Wijzigt de status van een actieve fiets naar uit omloop
     *
     * @param regnr registratienummer van de te wijzigen fiets
     */
    @Override
    public void wijzigenActiefNaarUitOmloop(Integer regnr) throws DBException, SQLException, ApplicationException  {
        //kijken of de fiets momenteel op actief staat
        Fiets fiets =  new Fiets();
        fiets = db.zoekFiets(regnr);
        
        //nagaan en controleren dat er zeker geen actieve ritten zijn met deze fiets
        ArrayList<Rit> rittenlijst = rdb.zoekActieveRittenVanFiets(fiets.getRegistratienummer());
        if(rittenlijst.isEmpty()){
            if(fiets.getStatus() == Status.actief){
                //de fiets is actief

                //toestand van de fiets wijzigen naar uit omloop
                db.wijzigenToestandFiets(regnr, Status.uit_omloop);
            }
            else{
                //de fiets was niet actief
                throw new ApplicationException("kan niet van actief naar omloop zetten, want fiets is momenteel niet actief");
            }
        }
        else{
            //er waren nog actieve ritten met de fiets
            throw new ApplicationException("er zijn nog steed actieve ritten met deze fiets, kan de fiets niet naar uitomloop zetten");
        }
    }

    
    /**
     * Wijzigt de status van een fiets in herstel naar actief
     *
     * @param regnr registratienummer van de te wijzigen fiets
     */
    @Override
    public void wijzigenHerstelNaarActief(Integer regnr) throws DBException, SQLException, ApplicationException  {
        //kijken of de fiets momenteel op herstel staat
        Fiets fiets =  new Fiets();
        fiets = db.zoekFiets(regnr);
        if(fiets.getStatus() == Status.herstel){
            //de fiets is in herstel

            //toestand van de fiets wijzigen naar actief
            db.wijzigenToestandFiets(regnr, Status.actief);
        }
        else{
            //de fiets was niet in herstel
            throw new ApplicationException("kan niet van in herstel naar actief zetten, want fiets is momenteel niet in herstel");
        }
    }

    /**
     * Wijzigt de status van een fiets in herstel naar uit omloop
     *
     * @param regnr registratienummer van de te wijzigen fiets
     */
    @Override
    public void wijzigenHerstelNaarUitOmloop(Integer regnr) throws SQLException, DBException, ApplicationException{
        //kijken of de fiets momenteel op in herstel staat
        Fiets fiets =  new Fiets();
        fiets = db.zoekFiets(regnr);
        if(fiets.getStatus() == Status.herstel){
            //de fiets is in herstel

            //toestand van de fiets wijzigen naar uit omloop
            db.wijzigenToestandFiets(regnr, Status.uit_omloop);
        }
        else{
            //de fiets was niet in herstel
            throw new ApplicationException("kan niet van in herstel naar omloop zetten, want fiets is momenteel niet in herstel");
        }
    }

    /**
     * Wijzigt de opmerking van een fiets
     *
     * @param regnr registratienymmer van de te wijzigen fiets
     * @param opmerking nieuwe opmerking die toegevoegd moet worden aan de fiets
     */
    @Override
    public void wijzigenOpmerkingFiets(Integer regnr, String opmerking) throws DBException, SQLException  {
        db.wijzigenOpmerkingFiets(regnr, opmerking);
    }

    /**
     * Zoekt een fiets
     *
     * @param registratienummer registratienummer van de te zoeken fiets
     * @return Fiets-object met alle gegevens van de gevonden fiets. Null indien
     * de fiets niet bestaat.
     */
    @Override
    public Fiets zoekFiets(Integer registratienummer) throws DBException, SQLException  {
        return db.zoekFiets(registratienummer);
    }

    /**
     * Zoekt alle fietsen.
     *
     * @return een arraylist met alle Fiets-objecten. Een lege lijst indien er
     * geen fietsen gevonden zijn.
     */
    @Override
    public ArrayList<Fiets> zoekAlleFietsen() throws DBException, SQLException  {
        return db.zoekAlleFietsen();
    }

   
}
