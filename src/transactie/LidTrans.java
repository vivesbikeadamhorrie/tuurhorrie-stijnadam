package transactie;

import databag.Lid;
import databag.Rit;
import database.LidDB;
import database.RitDB;
import exception.ApplicationException;
import exception.DBException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class LidTrans implements InterfaceLidTrans {

    private LidDB db = new LidDB();
    private RitDB rdb = new RitDB();
    
    /**
     * Voegt een lid toe
     *
     * @param l lid dat toegevoegd moet worden
     */
    @Override
    public void toevoegenLid(Lid l) throws ApplicationException, DBException, SQLException  {
        if(l == null){throw new ApplicationException("er werd geen lid megegeven");} //het megegeven lid is null
        
        //zijn alle nodige velden ingevuld?, rijksregisternummer, voornaam, naam, geslacht, tlnr, emailadres, start lidmaatschap
        if(l.getRijksregisternummer() == null ){
            throw new ApplicationException("het rijksregisternummer is leeg");
        }
        if(l.getVoornaam() == null || l.getVoornaam().trim().length() == 0){
            throw new ApplicationException("de voornaam is leeg");
        }
        if(l.getNaam() == null || l.getNaam().trim().length() == 0){
            throw new ApplicationException("de naam is leeg");
        }
        if(l.getGeslacht() == null || l.getGeslacht().toString().length() == 0){
            throw new ApplicationException("het geslacht is leeg");
        }
        if(l.getTelnr() == null || l.getTelnr().trim().length() == 0){
            throw new ApplicationException("het telefoonnummer is leeg");
        }
        if(l.getEmailadres()== null || l.getEmailadres().trim().length() == 0){
            throw new ApplicationException("het emailadres is leeg");
        }
        if(l.getStart_lidmaatschap() == null ){
            throw new ApplicationException("de startdatum is leeg");
        }
        
        //bestaat er al een lid met dit rijksregisternummer?
        if(db.zoekLid(l.getRijksregisternummer()) != null){
            throw new ApplicationException("er is al een lid met dit rijksregisternummer");
        }
        
        
        //het lid toevoegen
        db.toevoegenLid(l); //-> dit kan DB en SQL Exceptions gooien
    }

    /**
     * Wijzigt een lid
     *
     * @param teWijzigenLid het nieuwe lid (rijksregisternummer kan niet gewijzigd worden)
     */
    @Override
    public void wijzigenLid(Lid teWijzigenLid) throws DBException, SQLException, ApplicationException  {
        //het te wijzigen lid uit de database halen m.b.v. zijn rijksregisternummer
        Lid origineelLid = db.zoekLid(teWijzigenLid.getRijksregisternummer());
        
        //de eerste rit van het te wijzigen lid gaan ophalen
        Integer eersteRitId = rdb.zoekEersteRitVanLid(origineelLid.getRijksregisternummer());
        Rit eersteRit = rdb.zoekRit(eersteRitId);
        
        //checken of de datum niet veranderd is naar een datum achter zijn eerste rit
        if(teWijzigenLid.getStart_lidmaatschap().isAfter(eersteRit.getStarttijd().toLocalDate())  ){
            throw new ApplicationException("de gewijzigde startdatum valt na de eerste rit van dit lid, dit mag niet");
        }
        
        //nagaan of eindlidmaatschap niet voor startlidmaatschap komt
        if(teWijzigenLid.getStart_lidmaatschap().isAfter(teWijzigenLid.getEinde_lidmaatschap())){
            throw new ApplicationException("datum eindlidmaatschap valt voor datum startlidmaatschap");
        }
        
        db.wijzigenLid(teWijzigenLid);
        
    }

    /**
     * Schrijft een lid uit.
     *
     * @param rr rijksregisternummer van het uit te schrijven lid
     */
    @Override
    public void uitschrijvenLid(String rr) throws DBException, SQLException, ApplicationException  {
        //het uit te schrijven lid opzoeken in db
        Lid Liduitschrijven = db.zoekLid(rr);
        
        //actieve ritten van dit lid gaan opzoeken
        ArrayList<Rit> ritten = new ArrayList<Rit>();
        ritten = rdb.zoekActieveRittenVanLid(rr);
        
        //kijken als er actieve ritten zijn, is dit het geval, dan mag het lid niet uitgeschreven worden
        if(!(ritten.isEmpty())){
            throw new ApplicationException("het lid heeft nog actieve ritten, en kan niet worden uitgeschreven");
        }
        
        //het lid uitschrijven
        db.uitschrijvenLid(rr);
        
    }

    /**
     * Zoekt een lid.
     *
     * @param rijksregisternummer het rijksregisternummer van het te zoeken lid
     * @return Lid-object met alle gegevens van het gevonden lid. Null indien
     * het lid niet bestaat.
     */
    @Override
    public Lid zoekLid(String rijksregisternummer) throws DBException, SQLException, ApplicationException  {
        return db.zoekLid(rijksregisternummer);
    }

    /**
     * Zoekt alle leden.
     *
     * @return een arraylist met alle Lid-objecten. Een lege lijst indien er
     * geen leden gevonden zijn.
     */
    @Override
    public ArrayList<Lid> zoekAlleLeden() throws DBException, SQLException, ApplicationException  {
        return db.zoekAlleLeden();
    }

}
