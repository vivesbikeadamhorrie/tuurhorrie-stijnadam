/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transactie;

import databag.Fiets;
import databag.Rit;
import database.FietsDB;
import database.LidDB;
import database.RitDB;
import datatype.Status;
import exception.ApplicationException;
import exception.DBException;
import java.sql.SQLException;
import java.util.ArrayList;


public class RitTrans implements InterfaceRitTrans {

    private RitDB rdb = new RitDB();
    private LidDB ldb = new LidDB();
    private FietsDB fdb = new FietsDB();
    
    /**
     * Voegt een rit toe
     *
     * @param rit rit die moet toegevoegd worden
     * @return id van de pas toegevoegde rit
     */
    @Override
    public Integer toevoegenRit(Rit r) throws ApplicationException, DBException, SQLException {
      
      //kijken of de rit niet null is
      if(r == null) {throw new ApplicationException("er werd geen rit meegegeven");}
      
      //kijken of de fiets wel actief is
      Fiets fiets = fdb.zoekFiets(r.getFietsRegistratienummer());
      if(fiets.getStatus() == Status.actief){
      //de fiets is actief en mag gebruikt worden
        //kijken of startijd is ingevuld
        if(r.getStarttijd()==null){throw new ApplicationException("de starttijd moet bestaan");  }
        //kijken of het een geldig rijkrsegisternummer is
        try {
          ldb.zoekLid(r.getLidRijksregisternummer());}
        catch(Exception e) {
        throw new ApplicationException("het rijksregisternummer bestaat niet");

        }
         //kijken of het een geldig registratienummer is
         if(r.getFietsRegistratienummer() == 0){
         throw new ApplicationException("het fietsregistratienummer is niet geldig");
         }

         //nagaan of er al geen ritten zijn met deze fiets
         ArrayList<Rit> rlijst = rdb.zoekActieveRittenVanFiets(r.getFietsRegistratienummer());
         if(rlijst.isEmpty()){
             //er zijn momenteel geen actieve ritten met deze fiets
             //nagaan of het lid niet al een fiets aan het huren is
             ArrayList<Rit> rrlijst = rdb.zoekActieveRittenVanLid(r.getLidRijksregisternummer());
             if(rrlijst.isEmpty()){
                //het lid is geen fiets aan het huren, en mag dus een fiets huren
                //de rit aanmaken
                int x = rdb.toevoegenRit(r);
                return x;
             }
             else{throw new ApplicationException("het lid is al een fiets aan het huren, een lid mag maximaal 1 fiets huren");}
         }
         else{throw new ApplicationException("de fiets is al verhuurd in een andere rit");}

         
       }
      //de fiets was niet actief
      throw new ApplicationException("de fiets is niet actief, en kan dus niet uitgeleend worden");
    }

    /**
     * Sluit een rit af.
     *
     * @param id id van de rit die afgesloten moet worden
     */
    @Override
    public void afsluitenRit(Integer id) throws  DBException, SQLException, ApplicationException {
     
         Rit r = rdb.zoekRit(id);
         rdb.afsluitenRit(r);
         
       
    }

    /**
     * Zoekt alle ritten.
     *
     * @return een arraylist met alle Rit-objecten. Een lege lijst indien er
     * geen ritten gevonden zijn.
     */
    @Override
    public ArrayList zoekAlleRitten() throws ApplicationException {
        ArrayList<Rit> list = new ArrayList<Rit>();
        try {
            list = rdb.zoekAlleRitten();
        } catch (Exception e) {
           throw new ApplicationException("Er ging iets mis met het opzoeken van de lijst"); 
        }
        
        
        return list;
    }

    /**
     *
     * Zoekt een rit
     *
     * @param ritID id van de te zoeken rit
     * @return Rit-object met alle gegevens van de gevonden rit. Null indien de
     * rit niet bestaat.
     */
    @Override
    public Rit zoekRit(Integer ritID) throws DBException, SQLException, ApplicationException {
       
       return rdb.zoekRit(ritID);
         }

    /**
     * Zoekt het id van de eerste rit van een lid
     *
     * @param rr rijksregeisternummer van het lid waarvan de eerste rit gezocht
     * wordt
     * @return id van de eerste rit van een lid
     */
    @Override
    public Integer zoekEersteRitVanLid(String rr) throws DBException, SQLException  {
      
       return rdb.zoekEersteRitVanLid(rr);
      
    }

    /**
     * Zoekt de actieve ritten van een lid (dus alle openstaande ritten op dit
     * moment van een lid).
     *
     * @param rr rijksregeisternummer van het lid waarvan de actieve ritten
     * worden gezocht
     * @return een arraylist van alle actieve ritten van een gegeven lid
     */
    @Override
    public ArrayList zoekActieveRittenVanLid(String rr) throws DBException, SQLException, ApplicationException  {
     
      return rdb.zoekActieveRittenVanLid(rr);
      
    }

    /**
     * Zoekt de actieve ritten van een fiets (dus alle openstaande ritten op dit
     * moment van een fiets).
     *
     * @param regnr registernummer van de fiets waarvan de actieve ritten worden
     * gezocht
     * @return een arraylist van alle actieve ritten van een gegeven fiets
     */
    @Override
    public ArrayList zoekActieveRittenVanFiets(Integer regnr) throws DBException, SQLException, ApplicationException  {
      return rdb.zoekActieveRittenVanFiets(regnr);
    }
}
