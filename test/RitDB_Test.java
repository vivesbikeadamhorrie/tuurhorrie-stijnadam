/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import databag.Fiets;
import databag.Lid;
import databag.Rit;
import database.FietsDB;
import database.LidDB;
import database.RitDB;
import database.connect.ConnectionManager;
import datatype.Geslacht;
import datatype.Rijksregisternummer;
import datatype.Standplaats;
import datatype.Status;
import exception.ApplicationException;
import exception.DBException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Tuur
 */
public class RitDB_Test {
    private Rit rit;
    private Lid lid;
    private Fiets fiets;
    private RitDB rdb;
    private FietsDB fdb;
    private LidDB ldb;
    
    public RitDB_Test() {
    }
    
    
    @Rule
    public ExpectedException thrown
            = ExpectedException.none();
    
   //methode om een rit te verwijderen
    public static void removeRit(int id) throws DBException {

        // connectie tot stand brengen (en automatisch sluiten)
        try (Connection conn = ConnectionManager.getConnection();) {
            // preparedStatement opstellen (en automtisch sluiten)
            try (PreparedStatement stmt = conn.prepareStatement(
                    "delete from rit where id = ?");) {
                stmt.setInt(1, id);
                // execute voert elke sql-statement uit, executeQuery enkel de select
                stmt.execute();
            } catch (SQLException sqlEx) {
                throw new DBException("SQL-exception in removeRit - statement" + sqlEx);
            }
        } catch (SQLException sqlEx) {
            throw new DBException(
                    "SQL-exception in remoceRit - connection" + sqlEx);
        }
    }
    //methode om een fiets te verwijderen
    public static void removeFiets(int id) throws DBException {

        // connectie tot stand brengen (en automatisch sluiten)
        try (Connection conn = ConnectionManager.getConnection();) {
            // preparedStatement opstellen (en automtisch sluiten)
            try (PreparedStatement stmt = conn.prepareStatement(
                    "delete from fiets where registratienummer = ?");) {
                stmt.setInt(1, id);
                // execute voert elke sql-statement uit, executeQuery enkel de select
                stmt.execute();
            } catch (SQLException sqlEx) {
                throw new DBException("SQL-exception in removeFiets - statement" + sqlEx);
            }
        } catch (SQLException sqlEx) {
            throw new DBException(
                    "SQL-exception in removeFiets - connection" + sqlEx);
        }
    }
    //methode om een lid te verwijderen
    public static void removeLid(String rr) throws DBException {

        // connectie tot stand brengen (en automatisch sluiten)
        try (Connection conn = ConnectionManager.getConnection();) {
            // preparedStatement opstellen (en automtisch sluiten)
            try (PreparedStatement stmt = conn.prepareStatement(
                    "delete from lid where rijksregisternummer = ?");) {
                stmt.setString(1, rr);
                // execute voert elke sql-statement uit, executeQuery enkel de select
                stmt.execute();
            } catch (SQLException sqlEx) {
                throw new DBException("SQL-exception in removeLid - statement" + sqlEx);
            }
        } catch (SQLException sqlEx) {
            throw new DBException(
                    "SQL-exception in removeLid - connection" + sqlEx);
        }
    }
    
    
    @Before
    public void setUp() throws ApplicationException {
        //lid van rit aanmaken
        lid = new Lid();
        lid.setRijksregisternummer(new Rijksregisternummer("30051400150"));
        lid.setVoornaam("tom");
        lid.setNaam("tommer");
        lid.setGeslacht(Geslacht.M);
        lid.setTelnr("0471234567");
        lid.setEmailadres("t@telenet.com");
        lid.setStart_lidmaatschap(LocalDate.now());
        
        //fiets van rit aanmaken
        fiets = new Fiets();
        fiets.setStatus(Status.actief);
        fiets.setStandplaats(Standplaats.Tielt);
        
        // rit aanmaken
        rit = new Rit();
        rit.setFietsRegistratienummer(2);
        rit.setLidRijksregisternummer(new Rijksregisternummer("30051400150"));
        rit.setStarttijd(LocalDateTime.now());
        
        //dbs aanmaken
        rdb = new RitDB();
        fdb = new FietsDB();
        ldb = new LidDB();
    }
    
    //standaard rit toevoegen
    @Test
    public void testToevoegenEnZoekenRit() throws DBException, SQLException, ApplicationException {
            //fiets en rit toevoegen
            ldb.toevoegenLid(lid);
            Integer fid = fdb.toevoegenFiets(fiets);
            rit.setFietsRegistratienummer(fid);
        try{

            
            //rit toevoegen
            rit.setRitID(rdb.toevoegenRit(rit));
            //rit opnieuw ophalen
            Rit ophaalRit = rdb.zoekRit(rit.getRitID());
            
            //toegevoegde rit vergelijken met opgehaalde rit
            assertEquals("FOUT: ritid niet correct",rit.getRitID(),ophaalRit.getRitID(),0);
            assertEquals("FOUT: startijd niet correct",rit.getStarttijd().toString().substring(0, rit.getStarttijd().toString().length() - 7),ophaalRit.getStarttijd().toString().substring(0,ophaalRit.getStarttijd().toString().length() - 3));
            assertEquals("FOUT: eindtijd niet correct",rit.getEindtijd(),ophaalRit.getEindtijd());
            assertEquals("FOUT: prijs niet correct",rit.getPrijs(),ophaalRit.getPrijs());
            assertEquals("FOUT: lid_rijksregisternummer niet correct",rit.getLidRijksregisternummer(),ophaalRit.getLidRijksregisternummer());
            assertEquals("FOUT: fiets_registratienummer niet correct",rit.getFietsRegistratienummer(),ophaalRit.getFietsRegistratienummer(),0);

        }
        finally{
            //rit weer verwijderen
            removeRit(rit.getRitID());
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //rit met een id, maar wordt niet opgenomen (id wordt door db gemaakt)
    @Test
    public void testToevoegenRitMetId() throws DBException, SQLException, ApplicationException{
        
        Integer x = null;
        //fiets en rit toevoegen
        ldb.toevoegenLid(lid);
        Integer fid = fdb.toevoegenFiets(fiets);
        rit.setFietsRegistratienummer(fid);
        try{
            rit.setRitID(-1);
            //rit toevoegen
            x = rdb.toevoegenRit(rit);

            //rit opnieuw ophalen op basis van het gegenereerde id
            Rit ophaalRit = rdb.zoekRit(x);

            //toegevoegde rit vergelijken met opgehaalde rit
            assertEquals("FOUT: ritid niet correct",x,ophaalRit.getRitID(),0);
            assertEquals("FOUT: startijd niet correct",rit.getStarttijd().toString().substring(0, rit.getStarttijd().toString().length() - 7),ophaalRit.getStarttijd().toString().substring(0,ophaalRit.getStarttijd().toString().length() - 3));
            assertEquals("FOUT: eindtijd niet correct",rit.getEindtijd(),ophaalRit.getEindtijd());
            assertEquals("FOUT: prijs niet correct",rit.getPrijs(),ophaalRit.getPrijs());
            assertEquals("FOUT: lid_rijksregisternummer niet correct",rit.getLidRijksregisternummer(),ophaalRit.getLidRijksregisternummer());
            assertEquals("FOUT: fiets_registratienummer niet correct",rit.getFietsRegistratienummer(),ophaalRit.getFietsRegistratienummer(),0);

        }
        finally{
            removeRit(x);
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //twee ritten met zelfde id toevoegen
    //heeft geen effect want DB genereert id
    @Test
    public void testToevoegenRitZelfdeId() throws DBException, SQLException, ApplicationException{
        Integer id1 = null;
        Integer id2 = null;
        //fiets en rit toevoegen
        ldb.toevoegenLid(lid);
        Integer fid = fdb.toevoegenFiets(fiets);
        rit.setFietsRegistratienummer(fid);
        try{
            //rit toevoegen
            id1 = rdb.toevoegenRit(rit);
            rit.setRitID(id1);
           
            
            
            //rit2 maken
            Rit rit2 = new Rit();
            rit2.setFietsRegistratienummer(2);
            rit2.setLidRijksregisternummer(new Rijksregisternummer("99031810104"));
            rit2.setStarttijd(LocalDateTime.now());
            rit2.setRitID(id1);
            
            //rit2 toevoegen
            id2 = rdb.toevoegenRit(rit2);
            //rit2 opnieuw ophalen
            Rit ophaalRit = rdb.zoekRit(id2);
            
            //rit2 vergelijken met opgehaalde rit
            //id van toegevoegde rit = id2
            //rit heeft nog originele id
            assertEquals("FOUT: ritid niet correct", id2, ophaalRit.getRitID(),0);
            assertFalse("FOUT: klantid niet correct",id1.equals(ophaalRit.getRitID()));
            
        }
        finally{
            //de 2 ritten verwijderen
            removeRit(id1);
            removeRit(id2);
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
        
                
    }
    
    //een rit proberen toevoegen zonder een startijd, wat niet mag
    @Test
    public void testToevoegenRitZonderStartijd() throws DBException, SQLException, ApplicationException{
        thrown.expect(DBException.class);
        //fiets en rit toevoegen
        ldb.toevoegenLid(lid);
        Integer fid = fdb.toevoegenFiets(fiets);
        rit.setFietsRegistratienummer(fid);
            try{
            //startijd leegmaken
            rit.setStarttijd(null);
            //rit toevoegen
            Integer x = rdb.toevoegenRit(rit); //--> zou fout moeten gooien
            }
            finally{
                //lid en fiets weer verwijderen
                removeFiets(fid);
                removeLid(lid.getRijksregisternummer());
            }
    }
    
    //een rit proberen toevoegen met ongeldig rijksregisternummer, wat niet mag
    @Test
    public void testToevoegenRitOngeldigRijksregisternummer() throws DBException, SQLException, ApplicationException{
        thrown.expect(ApplicationException.class);
        //fiets en rit toevoegen
        ldb.toevoegenLid(lid);
        Integer fid = fdb.toevoegenFiets(fiets);
        rit.setFietsRegistratienummer(fid);
        Integer x = null;
        try{
            //rijksregisternummer ongledig maken
            rit.setLidRijksregisternummer(new Rijksregisternummer("1234"));
            //rit toevoegen
            x = rdb.toevoegenRit(rit); //--> zou fout moeten gooien
            }
        finally{
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //een rit proberen toevoegen met rijksregisternummer die null is
    @Test
    public void testToevoegenRitRijksregisternummerNull() throws DBException, SQLException, ApplicationException{
        thrown.expect(DBException.class);
        //fiets en rit toevoegen
        ldb.toevoegenLid(lid);
        Integer fid = fdb.toevoegenFiets(fiets);
        rit.setFietsRegistratienummer(fid);
        try{
            //rijksregisternummer ongledig maken
            rit.setLidRijksregisternummer(null);
            //rit toevoegen
            Integer x = rdb.toevoegenRit(rit); //--> zou fout moeten gooien
            }
        finally{
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //een rit proberen toevoegen met een ongeldig fiets nummer
    @Test
    public void testToevoegenRitOngeldigFietsNummer() throws DBException, SQLException{
        thrown.expect(DBException.class);
        //fiets en rit toevoegen
        ldb.toevoegenLid(lid);
        Integer fid = fdb.toevoegenFiets(fiets);
        rit.setFietsRegistratienummer(fid);
        Integer x = null;
        try{
            //registratienummer ongledig maken
            rit.setFietsRegistratienummer(-1);
            //rit toevoegen
            x = rdb.toevoegenRit(rit); //--> zou fout moeten gooien
            }
        finally{
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //een rit afsluiten
    @Test
    public void testRitAfsluiten() throws DBException, SQLException, ApplicationException{
        //fiets en rit toevoegen
            ldb.toevoegenLid(lid);
            Integer fid = fdb.toevoegenFiets(fiets);
            rit.setFietsRegistratienummer(fid);
        try{
            //rit toevoegen
            rit.setRitID(rdb.toevoegenRit(rit));
            //rit afsluiten
            rdb.afsluitenRit(rit);
            //rit opnieuw ophalen
            Rit ophaalRit = rdb.zoekRit(rit.getRitID());
            
            //toegevoegde rit vergelijken met opgehaalde rit
            assertEquals("FOUT: ritid niet correct",rit.getRitID(),ophaalRit.getRitID(),0);
            assertEquals("FOUT: startijd niet correct",rit.getStarttijd().toString().substring(0, rit.getStarttijd().toString().length() - 7),ophaalRit.getStarttijd().toString().substring(0,ophaalRit.getStarttijd().toString().length() - 3));
            assertEquals("FOUT: prijs niet correct",rit.getPrijs(),ophaalRit.getPrijs());
            assertEquals("FOUT: lid_rijksregisternummer niet correct",rit.getLidRijksregisternummer(),ophaalRit.getLidRijksregisternummer());
            assertEquals("FOUT: fiets_registratienummer niet correct",rit.getFietsRegistratienummer(),ophaalRit.getFietsRegistratienummer(),0);
            assertFalse("FOUT: de eindtijd staat nog steeds op null",ophaalRit.getEindtijd() == null);

        }
        finally{
            //rit weer verwijderen
            removeRit(rit.getRitID());
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //kijken of zoekalleritten wel een arraylist teruggeeft
    @Test
    public void testZoekAlleRitten() throws DBException, SQLException, ApplicationException{
        //fiets en rit toevoegen
            ldb.toevoegenLid(lid);
            Integer fid = fdb.toevoegenFiets(fiets);
            rit.setFietsRegistratienummer(fid);
        try{
            //rit toevoegen
            rit.setRitID(rdb.toevoegenRit(rit));
            //ritten in arraylist stoppen
            ArrayList<Rit> rlijst = rdb.zoekAlleRitten();
            
            //kijken of de arrylist niet null, of leeg is.
            assertFalse("FOUT: de arraylist die returned werd, is null",rlijst == null );
            assertTrue("FOUT: de arraylist heeft een incorrecte grootte", rlijst.size() > 0);

        }
        finally{
            //rit weer verwijderen
            removeRit(rit.getRitID());
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //zoek eerste rit van lid
    @Test
    public void testZoekEersteRitVanLid() throws DBException, SQLException, ApplicationException{
        //fiets en rit toevoegen
            ldb.toevoegenLid(lid);
            Integer fid = fdb.toevoegenFiets(fiets);
            rit.setFietsRegistratienummer(fid);
        try{
            //rit toevoegen
            rit.setRitID(rdb.toevoegenRit(rit));
            //eerste rit van lid zoeken
            Integer ophaalId = rdb.zoekEersteRitVanLid(rit.getLidRijksregisternummer());
            Rit ophaalRit = rdb.zoekRit(ophaalId);
            
            //toegevoegde rit vergelijken met opgehaalde rit
            assertEquals("FOUT: ritid niet correct",rit.getRitID(),ophaalRit.getRitID(),0);
            assertEquals("FOUT: startijd niet correct",rit.getStarttijd().toString().substring(0, rit.getStarttijd().toString().length() - 7),ophaalRit.getStarttijd().toString().substring(0,ophaalRit.getStarttijd().toString().length() - 3));
            assertEquals("FOUT: eindtijd niet correct",rit.getEindtijd(),ophaalRit.getEindtijd());
            assertEquals("FOUT: prijs niet correct",rit.getPrijs(),ophaalRit.getPrijs());
            assertEquals("FOUT: lid_rijksregisternummer niet correct",rit.getLidRijksregisternummer(),ophaalRit.getLidRijksregisternummer());
            assertEquals("FOUT: fiets_registratienummer niet correct",rit.getFietsRegistratienummer(),ophaalRit.getFietsRegistratienummer(),0);

        }
        finally{
            //rit weer verwijderen
            removeRit(rit.getRitID());
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //actieve ritten zoeken van lid
    @Test
    public void testZoekActieveRittenVanLid() throws DBException, SQLException, ApplicationException{
        //fiets en rit toevoegen
            ldb.toevoegenLid(lid);
            Integer fid = fdb.toevoegenFiets(fiets);
            rit.setFietsRegistratienummer(fid);
        try{
            //rit toevoegen
            rit.setRitID(rdb.toevoegenRit(rit));
            //actieve ritten van lid opvragen
            ArrayList<Rit> ophaalRitten = rdb.zoekActieveRittenVanLid(rit.getLidRijksregisternummer());
            
            assertTrue("FOUT: de arraylist moet enkel 1 rit bevatten",ophaalRitten.size() == 1 );
            
        }
        finally{
            //rit weer verwijderen
            removeRit(rit.getRitID());
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
    
    //actieve ritten van fiets
    @Test
    public void testZoekActieveRittenVanFiets() throws DBException, SQLException, ApplicationException{
        //fiets en rit toevoegen
            ldb.toevoegenLid(lid);
            Integer fid = fdb.toevoegenFiets(fiets);
            rit.setFietsRegistratienummer(fid);
        try{
            //rit toevoegen
            rit.setRitID(rdb.toevoegenRit(rit));
            //alle actieve ritten van fiets opvragen
            ArrayList<Rit> ophaalRitten = rdb.zoekActieveRittenVanFiets(fid);
            
            assertTrue("FOUT: de arraylist moet enkel 1 rit bevatten",ophaalRitten.size() == 1 );
            
        }
        finally{
            //rit weer verwijderen
            removeRit(rit.getRitID());
            //lid en fiets weer verwijderen
            removeFiets(fid);
            removeLid(lid.getRijksregisternummer());
        }
    }
}
