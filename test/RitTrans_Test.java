/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import databag.Fiets;
import databag.Lid;
import databag.Rit;
import datatype.Geslacht;
import datatype.Rijksregisternummer;
import datatype.Standplaats;
import datatype.Status;
import exception.ApplicationException;
import exception.DBException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import transactie.FietsTrans;
import transactie.LidTrans;
import transactie.RitTrans;

/**
 *
 * @author Tuur
 */
public class RitTrans_Test {
     private Fiets f = new Fiets();
      private Lid l = new Lid();
      private Rit r = new Rit();
       private RitTrans rt = new RitTrans();
       private FietsTrans ft = new FietsTrans();
       private LidTrans lt = new LidTrans();
        @Rule
    public ExpectedException thrown
            = ExpectedException.none();
    public RitTrans_Test() {
    }

    
    @Before
    public void setUp() throws ApplicationException, DBException, SQLException {
     

    f.setStatus(Status.actief);
    f.setRegistratienummer(1);
   f.setOpmerking("");
   f.setStandplaats(Standplaats.Tielt);

    Rijksregisternummer rr = new Rijksregisternummer("93051822361");
    l.setRijksregisternummer(rr);
    l.setEmailadres("lkj");
    l.setGeslacht(Geslacht.M);
    l.setNaam("test");
    l.setTelnr("0123");
    l.setVoornaam("tester");
    
    r.setFietsRegistratienummer(f.getRegistratienummer());
    r.setLidRijksregisternummer(rr);

    }
    @Test
    public void testToevoegenRitMetStartTijd() throws DBException, SQLException, ApplicationException, ApplicationException{
      r.setStarttijd(LocalDateTime.now());
      Rit r2 = new Rit();
      lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
      try {
      rt.toevoegenRit(r);
          r2 = rt.zoekRit(r.getRitID());
      assertEquals("FOUT: lid is niet correct", r2.getLidRijksregisternummer(), r.getLidRijksregisternummer());
      assertEquals("FOUT: fiets is niet correct", r2.getFietsRegistratienummer(), r.getFietsRegistratienummer());
      assertEquals("FOUT: rit id is niet correct", r2.getRitID(), r.getRitID());
      assertEquals("FOUT: starttijd is niet correct", r2.getStarttijd(), r.getStarttijd());
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }   
    @Test
    public void testToevoegenZonderStartTijd() throws ApplicationException, DBException, SQLException {
    thrown.expect(ApplicationException.class);
  
      lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
      try {
      rt.toevoegenRit(r);
    
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }
    @Test
    public void testToevoegenZonderFiets() throws ApplicationException, DBException, SQLException {
       thrown.expect(ApplicationException.class);

      lt.toevoegenLid(l);
       
      try {
      rt.toevoegenRit(r);
    
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }
    @Test 
    public void testToevoegenZonderLid() throws ApplicationException, DBException, SQLException {
       thrown.expect(ApplicationException.class);
     
      
       ft.toevoegenFiets(f);
      try {
      rt.toevoegenRit(r);
    
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }
    @Test
    public void testToevoegenFietsHerstel() throws ApplicationException, DBException, SQLException {
       thrown.expect(ApplicationException.class);
      
      f.setStatus(Status.herstel);
        
      lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
      try {
      rt.toevoegenRit(r);
    
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }
    @Test
    public void testToevoegenFietsUItOmloop() throws ApplicationException, DBException, SQLException {
     thrown.expect(ApplicationException.class);
     
      f.setStatus(Status.uit_omloop);
        
      lt.toevoegenLid(l);
      
       Integer fid = ft.toevoegenFiets(f);
      try {
      rt.toevoegenRit(r);
    
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }
    @Test 
    public void testToevoegenFietsActief() throws ApplicationException, DBException, SQLException{
    thrown.expect(ApplicationException.class);
      Rit r2 = new Rit();
       Rijksregisternummer rr = new Rijksregisternummer("93051822362");
      lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
       r2.setFietsRegistratienummer(f.getRegistratienummer());
       r2.setLidRijksregisternummer(rr);
      try {
      rt.toevoegenRit(r);
      rt.toevoegenRit(r2);
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }
    @Test
    public void testToevoegenLidActief() throws ApplicationException, DBException, SQLException, SQLException, SQLException {
      thrown.expect(ApplicationException.class);
      Rit r2 = new Rit();
       Rijksregisternummer rr = new Rijksregisternummer("93051822361");
      lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
       r2.setFietsRegistratienummer(f.getRegistratienummer() + 1);
       r2.setLidRijksregisternummer(rr);
      try {
      rt.toevoegenRit(r);
      rt.toevoegenRit(r2);
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }
    @Test
    public void testAfsluitenRit() throws ApplicationException, DBException, SQLException {

      lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
     
      try {
      rt.toevoegenRit(r);
      rt.afsluitenRit(r.getRitID());
      assertEquals("FOUT: DE EINDTIJD WERD NIET AANGPAST",r.getEindtijd(), null);
      }
      finally {
        lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
    }
    }
@Test
public void testZoekRitten() throws ApplicationException  {
        ArrayList<Rit> list = new ArrayList<Rit>();
        list = rt.zoekAlleRitten();
        assertEquals("FOUT: ER WERD GEEN LIJST GEKREGEN",list, null);
}
   @Test
   public void testzoekRit() throws DBException, ApplicationException, SQLException{
   try {
    lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
       rt.toevoegenRit(r);
       assertEquals("FOUT: ER WERD GEEN RIT GEKREGEN", rt.zoekRit(r.getRitID()), null );
   }finally {
       lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
   }
   }
   
   @Test
   public void testZoekEersteRitVanLid() throws ApplicationException, DBException, SQLException {
   try {
    lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
       rt.toevoegenRit(r);
       assertEquals("FOUT: ER WERD GEEN ID TERUGGEKRGEN", rt.zoekEersteRitVanLid(l.getRijksregisternummer()),null);
   }finally {
   lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
   }
   }
   @Test
   public void testZoekAlleRittenVanLid() throws ApplicationException, DBException, SQLException {
   try {
    lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
       rt.toevoegenRit(r);
       assertEquals("FOUT: GEEN ARRAYLIST GEKREGEN VOOR LID", rt.zoekActieveRittenVanLid(l.getRijksregisternummer()),null);
   }finally {
   lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
   }
   
   }
   @Test
   public void testzoekActieveRittenVanFiets() throws ApplicationException, DBException, SQLException {
      try {
    lt.toevoegenLid(l);
       ft.toevoegenFiets(f);
       rt.toevoegenRit(r);
       assertEquals("FOUT: GEEN ARRAYLIST GEKREGEN VOOR FIETS", rt.zoekActieveRittenVanFiets(f.getRegistratienummer()),null);
   }finally {
   lt.uitschrijvenLid(l.getRijksregisternummer());
        ft.wijzigenActiefNaarUitOmloop(f.getRegistratienummer());
        rt.afsluitenRit(r.getRitID());
   }
   }
}
